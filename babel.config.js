module.exports = (api) => {
  api.cache.invalidate(() => process.env.NODE_ENV === 'production');
  return {
    plugins: [
      '@babel/plugin-transform-runtime',
      'babel-plugin-styled-components',
    ],
    presets: ['@babel/preset-env', '@babel/preset-react'],
  };
};
