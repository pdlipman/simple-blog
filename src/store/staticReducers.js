import { firebaseReducer } from 'react-redux-firebase';

import authReducer from 'providers/Firebase/Auth/reducer';
import loginReducer from 'providers/Firebase/Login/reducer';
import postReducer from 'providers/Firebase/Post/reducer';
import routingReducer from 'providers/Routing/reducer';

const staticReducers = {
  auth: authReducer,
  firebase: firebaseReducer,
  login: loginReducer,
  post: postReducer,
  routing: routingReducer,
};

export default staticReducers;
