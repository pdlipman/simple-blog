import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';

import rootSaga from './rootSaga';
import staticReducers from './staticReducers';

const createReducer = (asyncReducers) => {
  return combineReducers({
    ...staticReducers,
    ...asyncReducers,
  });
};

const createSagaInjector = (runSaga, staticSagas, store) => {
  const injectedSagas = new Map();
  const isInjected = (key) => injectedSagas.has(key);

  const injectSaga = (key, saga) => {
    if (!isInjected(key)) {
      const task = runSaga(saga);
      injectedSagas.set(key, task);
      // eslint-disable-next-line no-param-reassign
      store.injectedSagas[key] = task;
    }
  };

  injectSaga('static', staticSagas);

  return injectSaga;
};

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware];

  const rootReducer = createReducer();

  const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(...middlewares)),
  );
  store.injectedReducers = {};
  store.injectedSagas = {};

  store.injectReducer = (key, asyncReducer) => {
    store.injectedReducers[key] = asyncReducer;
    store.replaceReducer(createReducer(store.injectedReducers));
  };

  store.injectSaga = createSagaInjector(sagaMiddleware.run, rootSaga, store);

  const persistor = persistStore(store);
  store.persistor = persistor;

  return store;
};

export default configureStore;
