// global providers
import { all, call, spawn } from 'redux-saga/effects';

import authSaga from 'providers/Firebase/Auth/saga';
import loginSaga from 'providers/Firebase/Login/saga';
import postSaga from 'providers/Firebase/Post/saga';

export function* helloSaga() {
  yield call(console.log, 'Root sagas loaded.');
}

export default function* rootSaga() {
  // const sagas = [helloSaga, formSaga];
  const sagas = [authSaga, helloSaga, loginSaga, postSaga];

  yield all(
    sagas.map((saga) =>
      spawn(function* sagaCombiner() {
        while (true) {
          try {
            yield call(saga);
            break;
          } catch (error) {
            yield call(console.error, error);
            break;
          }
        }
      }),
    ),
  );
}
