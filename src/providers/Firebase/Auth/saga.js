import { call, fork, put, select, take, takeLatest } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';

import firebase from 'providers/Firebase';
import {
  authUserAvailableAction,
  authUserNewClaimsAvailableAction,
  authUserUnavailableAction,
} from './actions';
import { AUTH_USER_AVAILABLE } from './constants';
import { makeSelectUser } from './selectors';

function createAuthChannel() {
  const authChannel = eventChannel((emit) => {
    const unsubscribe = firebase.auth().onAuthStateChanged(
      (user) => emit({ user }),
      (error) => emit({ error }),
    );
    return unsubscribe;
  });
  return authChannel;
}

function* watchForFirebaseAuth() {
  const authChannel = yield call(createAuthChannel);
  while (true) {
    debugger;
    const { user } = yield take(authChannel);
    if (user) {
      debugger;
      const { claims } = yield call([user, user.getIdTokenResult]);
      yield put(authUserAvailableAction({ claims, user }));
    } else {
      debugger;
      yield put(authUserUnavailableAction());
    }
  }
}

function createClaimChannel(uid) {
  const claimChannel = eventChannel((emit) => {
    const document = firebase.firestore().collection('user-claims').doc(uid);
    const unsubscribe = document.onSnapshot(
      (snapshot) => emit({ claims: snapshot.data() }),
      (error) => emit({ error }),
    );

    return unsubscribe;
  });
  return claimChannel;
}

function* watchForClaimUpdate() {
  const user = yield select(makeSelectUser);
  const { claims: userClaims } = yield call([user, user.getIdTokenResult]);
  const claimChannel = yield call(createClaimChannel, user.uid);
  while (true) {
    const { claims: newClaims, error } = yield take(claimChannel);

    if (newClaims) {
      const updatedClaims = Object.keys(newClaims)
        .filter((key) => {
          return (
            key !== '_lastCommitted' &&
            !userClaims[key] &&
            userClaims[key] !== newClaims[key]
          );
        })
        .map((key) => {
          return { [key]: newClaims[key] };
        });

      if (updatedClaims.length > 0) {
        const claims = Object.assign({}, ...updatedClaims);
        yield put(authUserNewClaimsAvailableAction({ claims }));
      }
    } else if (error) {
      claimChannel.close();
    }
  }
}

export default function* formSaga() {
  yield call(console.log, 'Auth sagas loaded.');
  yield fork(watchForFirebaseAuth);
  yield takeLatest(AUTH_USER_AVAILABLE, watchForClaimUpdate);
}
