import {
  AUTH_STATE_CHANGED,
  AUTH_USER_AVAILABLE,
  AUTH_USER_NEW_CLAIMS_AVAILABLE,
  AUTH_USER_UNAVAILABLE,
} from './constants';

export function authStateChangedAction() {
  return {
    type: AUTH_STATE_CHANGED,
  };
}

export function authUserAvailableAction({ claims, user }) {
  return {
    type: AUTH_USER_AVAILABLE,
    claims,
    user,
  };
}

export function authUserNewClaimsAvailableAction({ claims }) {
  return {
    type: AUTH_USER_NEW_CLAIMS_AVAILABLE,
    claims,
  };
}

export function authUserUnavailableAction() {
  return {
    type: AUTH_USER_UNAVAILABLE,
  };
}
