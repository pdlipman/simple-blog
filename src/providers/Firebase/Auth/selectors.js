import { get } from 'lodash';
import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const selectAuth = (state) => get(state, 'auth', initialState);

export const makeSelectUser = createSelector(selectAuth, (auth) =>
  get(auth, 'user'),
);

export const makeSelectUserId = createSelector(makeSelectUser, (user) =>
  get(user, 'uid'),
);
