import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import {
  AUTH_STATE_CHANGED,
  AUTH_USER_AVAILABLE,
  AUTH_USER_NEW_CLAIMS_AVAILABLE,
  AUTH_USER_UNAVAILABLE,
} from './constants';

const authPersistConfig = {
  debug: process.env.DEBUG,
  key: 'auth',
  storage,
};

export const initialState = {
  claims: null,
  status: 'auth loaded',
  user: { uid: null },
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_STATE_CHANGED: {
      const status = 'changed';
      return { ...state, status };
    }
    case AUTH_USER_AVAILABLE: {
      const { claims, user } = action;
      const status = 'user available';
      return { ...state, claims, status, user };
    }
    case AUTH_USER_NEW_CLAIMS_AVAILABLE: {
      const { claims: currentClaims } = state;
      const { claims: newClaims } = action;
      const status = 'claims updated';
      return { ...state, claims: { ...currentClaims, ...newClaims }, status };
    }
    case AUTH_USER_UNAVAILABLE: {
      const { claims } = initialState;
      const status = 'user unavailable';
      const { user } = initialState;
      return { ...state, claims, status, user };
    }
    default:
      return state;
  }
};

export default persistReducer(authPersistConfig, authReducer);
