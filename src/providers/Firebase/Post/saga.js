import { put, take, takeLatest } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from 'providers/Firebase';

import { postsErrorAction, postsSuccessAction } from './actions';
import { POSTS_REQUEST } from './constants';

function* getPosts() {
  const documents = firebase
    .firestore()
    .collection('posts')
    .orderBy('datetime');
  const postChannel = eventChannel((emit) => {
    const unsubscribe = documents.onSnapshot(
      (snapshot) => emit({ snapshot }),
      (error) => emit({ error }),
    );

    return unsubscribe;
  });

  while (true) {
    const { snapshot, error } = yield take(postChannel);
    if (snapshot) {
      const posts = [];
      snapshot.forEach((doc) => posts.push(doc.data()));
      yield put(postsSuccessAction({ posts }));
    } else if (error) {
      yield put(postsErrorAction({ error }));
      postChannel.close();
    }
  }
}

export default function* postSaga() {
  yield takeLatest(POSTS_REQUEST, getPosts);
}
