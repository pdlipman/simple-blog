import { get } from 'lodash';
import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const selectPost = (state) => get(state, 'post', initialState);

export const makeSelectPosts = createSelector(selectPost, (post) =>
  get(post, 'posts'),
);

export const makeSelectPostById = () =>
  createSelector(
    makeSelectPosts,
    (_, slug) => slug,
    (posts, slug) => posts.find((post) => post.slug === slug),
  );
