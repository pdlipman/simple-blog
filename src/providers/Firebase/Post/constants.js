const POSTS_ERROR = 'providers/Firebase/Post/POSTS_ERROR';
const POSTS_REQUEST = 'providers/Firebase/Post/POSTS_REQUEST';
const POSTS_SUCCESS = 'providers/Firebase/Post/POSTS_SUCCESS';

const postsStatus = {
  [POSTS_ERROR]: 'posts error',
  [POSTS_REQUEST]: 'posts requested',
  [POSTS_SUCCESS]: 'posts succeeded',
};

export { POSTS_ERROR, POSTS_REQUEST, POSTS_SUCCESS, postsStatus };
