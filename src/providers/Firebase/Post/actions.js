import {
  POSTS_ERROR,
  POSTS_REQUEST,
  POSTS_SUCCESS,
  postsStatus,
} from './constants';

export function postsErrorAction({ error }) {
  return {
    type: POSTS_ERROR,
    status: postsStatus[POSTS_ERROR],
    error,
  };
}

export function postsRequestAction() {
  return {
    type: POSTS_REQUEST,
    status: postsStatus[POSTS_REQUEST],
  };
}

export function postsSuccessAction({ posts }) {
  return {
    type: POSTS_SUCCESS,
    status: postsStatus[POSTS_SUCCESS],
    posts,
  };
}
