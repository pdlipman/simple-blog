import { POSTS_ERROR, POSTS_REQUEST, POSTS_SUCCESS } from './constants';

export const initialState = {
  posts: [],
  status: 'posts loaded',
};

const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case POSTS_ERROR: {
      const { status } = action;
      return { ...state, status };
    }
    case POSTS_REQUEST: {
      const { status } = action;
      return { ...state, status };
    }
    case POSTS_SUCCESS: {
      const { posts, status } = action;
      return { ...state, posts, status };
    }
    default:
      return state;
  }
};

export default postReducer;
