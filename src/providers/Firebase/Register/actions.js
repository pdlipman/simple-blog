import {
  REGISTER_CANCELLED,
  REGISTER_ERROR,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  registrationStatus,
} from './constants';

export function registerCancelledAction() {
  return {
    type: REGISTER_CANCELLED,
    status: registrationStatus[REGISTER_CANCELLED],
  };
}

export function registerErrorAction(error) {
  return {
    type: REGISTER_ERROR,
    status: registrationStatus[REGISTER_ERROR],
    error,
  };
}

export function registerRequestAction({ email, password }) {
  return {
    type: REGISTER_REQUEST,
    status: registrationStatus[REGISTER_REQUEST],
    email,
    password,
  };
}

export function registerSuccessAction() {
  return {
    type: REGISTER_SUCCESS,
    status: registrationStatus[REGISTER_SUCCESS],
  };
}
