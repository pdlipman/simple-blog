import { call, cancelled, fork, put, take } from 'redux-saga/effects';

import firebase from 'providers/Firebase';

import { registerCancelledAction, registerErrorAction } from './actions';
import { REGISTER_REQUEST } from './constants';

function* register(email, password) {
  try {
    yield call(
      [firebase.auth(), firebase.auth().createUserWithEmailAndPassword],
      email,
      password,
    );
  } catch (error) {
    yield call(registerErrorAction, 'error');
  } finally {
    if (yield cancelled()) {
      yield put(registerCancelledAction());
    }
  }
}

function* registerFlow() {
  while (true) {
    const { email, password } = yield take(REGISTER_REQUEST);
    yield fork(register, email, password);
  }
}

export default function* registerSaga() {
  yield call(console.log, 'Register sagas loaded.');
  yield fork(registerFlow);
}
