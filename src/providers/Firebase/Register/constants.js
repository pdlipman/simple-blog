const REGISTER_CANCELLED = 'providers/Firebase/Register/REGISTER_CANCELLED';
const REGISTER_ERROR = 'providers/Firebase/Register/REGISTER_ERROR';
const REGISTER_REQUEST = 'providers/Firebase/Register/REGISTER_REQUEST';
const REGISTER_SUCCESS = 'providers/Firebase/Register/REGISTER_SUCCESS';

const registrationStatus = {
  REGISTER_CANCELLED: 'registration cancelled',
  REGISTER_ERROR: 'registration error',
  REGISTER_REQUEST: 'registration requested',
  REGISTER_SUCCESS: 'registration succeeded',
};
export {
  REGISTER_CANCELLED,
  REGISTER_ERROR,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  registrationStatus,
};
