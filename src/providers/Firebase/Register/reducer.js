import {
  REGISTER_CANCELLED,
  REGISTER_ERROR,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
} from './constants';

const initialState = {
  error: null,
  status: 'registration loaded',
};

const registerReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_CANCELLED: {
      const { status } = action;
      return { ...state, status };
    }
    case REGISTER_ERROR: {
      const { error, status } = action;
      return { ...state, error, status };
    }
    case REGISTER_REQUEST: {
      const { status } = action;
      return { ...state, status };
    }
    case REGISTER_SUCCESS: {
      const { status } = action;
      return { ...state, status };
    }
    default:
      return state;
  }
};

export default registerReducer;
