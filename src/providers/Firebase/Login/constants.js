const namespace = 'providers/Firebase/Login';
const LOGIN_CANCELLED = `${namespace}/LOGIN_CANCELLED`;
const LOGIN_ERROR = `${namespace}/LOGIN_ERROR`;
const LOGIN_REQUEST = `${namespace}/LOGIN_REQUEST`;
const LOGIN_SUCCESS = `${namespace}/LOGIN_SUCCESS`;

const LOGOUT = `${namespace}/LOGOUT`;

const loginStatus = {
  [LOGIN_CANCELLED]: 'login cancelled',
  [LOGIN_ERROR]: 'login error',
  [LOGIN_REQUEST]: 'login requested',
  [LOGIN_SUCCESS]: 'login succeeded',
  [LOGOUT]: 'logged out',
};
export {
  LOGIN_CANCELLED,
  LOGIN_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  loginStatus,
  LOGOUT,
};
