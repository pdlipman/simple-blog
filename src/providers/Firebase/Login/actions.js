import {
  LOGIN_CANCELLED,
  LOGIN_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  loginStatus,
  LOGOUT,
} from './constants';

export function loginCancelledAction() {
  return {
    type: LOGIN_CANCELLED,
    status: loginStatus[LOGIN_CANCELLED],
  };
}

export function loginErrorAction({ error }) {
  return {
    type: LOGIN_ERROR,
    status: loginStatus[LOGIN_ERROR],
    error,
  };
}

export function loginRequestAction({ email, password }) {
  return {
    type: LOGIN_REQUEST,
    status: loginStatus[LOGIN_REQUEST],
    email,
    password,
  };
}

export function loginSuccessAction() {
  return {
    type: LOGIN_SUCCESS,
    status: loginStatus[LOGIN_SUCCESS],
  };
}

export function logoutAction() {
  return {
    type: LOGOUT,
    status: loginStatus[LOGOUT],
  };
}
