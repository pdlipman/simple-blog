import {
  LOGIN_CANCELLED,
  LOGIN_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGOUT,
} from './constants';

const initialState = {
  error: null,
  status: 'login loaded',
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_CANCELLED: {
      const { status } = action;
      return { ...state, status };
    }
    case LOGIN_ERROR: {
      const { error, status } = action;
      return { ...state, error, status };
    }
    case LOGIN_REQUEST: {
      const { status } = action;
      return { ...state, status };
    }
    case LOGIN_SUCCESS: {
      const { status } = action;
      return { ...state, status };
    }
    case LOGOUT: {
      const { status } = action;
      return { ...state, status };
    }
    default:
      return state;
  }
};

export default loginReducer;
