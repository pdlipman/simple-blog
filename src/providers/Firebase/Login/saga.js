import { call, fork, put, take, takeLatest } from 'redux-saga/effects';

import firebase from 'providers/Firebase';
import { loginErrorAction, loginSuccessAction } from './actions';
import { LOGIN_REQUEST, LOGOUT } from './constants';

function* login({ email, password }) {
  try {
    const auth = firebase.auth();
    yield call([auth, auth.signInWithEmailAndPassword], email, password);

    yield put(loginSuccessAction());
  } catch (error) {
    yield put(loginErrorAction({ error }));
  }
}

function* loginFlow() {
  while (true) {
    const request = yield take(LOGIN_REQUEST);
    const { email, password } = request;

    yield fork(login, { email, password });
  }
}

function* logout() {
  try {
    const auth = firebase.auth();
    yield call([auth, auth.signOut]);
  } catch (error) {
    yield put(loginErrorAction({ error }));
  }
}

function* logoutFlow() {
  yield takeLatest(LOGOUT, logout);
}

export default function* loginSaga() {
  yield fork(loginFlow);
  yield fork(logoutFlow);
}
