import { get } from 'lodash';
import { createSelector } from 'reselect';
import { initialState } from './reducer';

export const selectRouting = (state) => get(state, 'routing', initialState);

export const makeSelectBack = createSelector(selectRouting, (routing) =>
  get(routing, 'back'),
);
