import { SET_BACK_ROUTE, USE_BACK_ROUTE } from './constants';

export function setBackRouteAction({ route }) {
  return {
    type: SET_BACK_ROUTE,
    route,
  };
}

export function useBackRouteAction() {
  return {
    type: USE_BACK_ROUTE,
  };
}
