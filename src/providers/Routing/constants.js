const namespace = 'providers/Routing';
export const SET_BACK_ROUTE = `${namespace}/SET_BACK_ROUTE`;
export const USE_BACK_ROUTE = `${namespace}/USE_BACK_ROUTE`;
