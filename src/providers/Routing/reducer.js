import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { HOME_ROUTE } from 'components/App/constants';

import { SET_BACK_ROUTE, USE_BACK_ROUTE } from './constants';

const routingPersistConfig = {
  debug: process.env.DEBUG,
  key: 'routing',
  storage,
};

export const initialState = {
  status: 'routing loaded',
  error: null,
  back: HOME_ROUTE,
};

const routingReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_BACK_ROUTE: {
      const { route } = action;
      const status = 'back route set';
      return { ...state, back: route, status };
    }
    case USE_BACK_ROUTE: {
      const back = HOME_ROUTE;
      const status = 'back route used';
      return { ...state, back, status };
    }
    default:
      return state;
  }
};

export default persistReducer(routingPersistConfig, routingReducer);
