import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { ReactReduxFirebaseProvider } from 'react-redux-firebase';
import firebase from 'providers/Firebase';

import App from 'components/App';

import configureStore from 'store/configureStore';

const store = configureStore();

ReactDOM.render(
  <Suspense fallback={null}>
    <Provider store={store}>
      <PersistGate persistor={store.persistor}>
        <ReactReduxFirebaseProvider
          config={{}}
          firebase={firebase}
          dispatch={store.dispatch}
        >
          <App />
        </ReactReduxFirebaseProvider>
      </PersistGate>
    </Provider>
  </Suspense>,
  document.getElementById('root'),
);
