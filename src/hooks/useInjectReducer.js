import { useContext, useEffect } from 'react';
import { ReactReduxContext } from 'react-redux';

export const useInjectReducer = ({ key, reducer }) => {
  const { store } = useContext(ReactReduxContext);
  useEffect(() => {
    store.injectReducer(key, reducer);
  }, []);
};

export default useInjectReducer;
