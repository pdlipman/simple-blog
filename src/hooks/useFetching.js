import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

/**
 * useFetching hook - dispatch action on component load
 * @param fetchActionCreator
 */
const useFetching = (fetchActionCreator) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchActionCreator);
  }, []);
};

export default useFetching;
