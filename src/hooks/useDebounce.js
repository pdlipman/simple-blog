import { useEffect, useRef } from 'react';

/**
 * useDebounce hook -
 *
 * @param callback - function to run
 * @param value - value to watch
 * @param delay - time to wait before executing callback function
 */
const useDebounce = (callback, value, delay = 2000) => {
  const firstRun = useRef(true);

  useEffect(() => {
    if (firstRun.current) {
      firstRun.current = false;
      return;
    }

    const timeout = setTimeout(() => {
      callback();
    }, delay);

    // eslint-disable-next-line consistent-return
    return () => clearTimeout(timeout);
  }, [value]);
};

export default useDebounce;
