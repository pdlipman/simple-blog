import { useContext, useEffect } from 'react';
import { ReactReduxContext } from 'react-redux';

const ejectSaga = (key, store) => {
  if (Reflect.has(store.injectedSagas, key)) {
    const task = store.injectedSagas[key];
    task.cancel();
  }
};

export const useInjectSaga = ({ key, saga }) => {
  const { store } = useContext(ReactReduxContext);
  useEffect(() => {
    store.injectSaga(key, saga);
    return () => ejectSaga(key, store);
  }, []);
};

export default useInjectSaga;
