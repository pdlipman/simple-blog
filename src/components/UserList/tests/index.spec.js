import React from 'react';
import { render } from '@testing-library/react';

import UserList from '..';

describe('UserList', () => {
  it('renders', () => {
    const { asFragment } = render(<UserList />);
    expect(asFragment()).toMatchSnapshot();
  });
});
