import React from 'react';
import { useDispatch } from 'react-redux';

import {
  Avatar,
  Button,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@material-ui/core';
import { AccountCircle as AccountCircleIcon } from '@material-ui/icons';

import i18n from 'i18n';
import { logoutAction } from 'providers/Firebase/Login/actions';

const UserList = () => {
  const dispatch = useDispatch();
  const handleLogout = () => {
    dispatch(logoutAction());
  };

  return (
    <List>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <AccountCircleIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText>User Menu</ListItemText>
      </ListItem>
      <Divider variant="middle" />
      <ListItem>
        <Button fullWidth onClick={handleLogout}>
          {i18n.t('logout', 'Logout')}
        </Button>
      </ListItem>
    </List>
  );
};

export default UserList;
