import React from 'react';
import { render } from '@testing-library/react';

import Form from '..';

describe('Form', () => {
  it('renders', () => {
    const { asFragment } = render(<Form />);
    expect(asFragment()).toMatchSnapshot();
  });
});
