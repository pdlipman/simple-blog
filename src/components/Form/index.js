import React from 'react';
import PropTypes from 'prop-types';

import { Field, Formik } from 'formik';
import styled from 'styled-components';

import { Avatar, Button, Container, Typography } from '@material-ui/core';
import { LockOutlined } from '@material-ui/icons';

import TextInput from '@bit/pdlipman.react-components.formik-text-input';
import PasswordInput from '@bit/pdlipman.react-components.formik-password-input';

import i18n from 'i18n';

import { PASSWORD_INPUT, TEXT_INPUT } from './constants';

const StyledContainer = styled(Container)`
  ${({ theme }) => `
    align-items: center;
    display: flex;
    flex-direction: column;
    margin-top: ${theme.spacing(4)}px;
  `}
`;

const StyledAvatar = styled(Avatar)`
  ${({ theme }) => `
    margin: ${theme.spacing(1)}px;
    background-color: ${theme.palette.primary.main};
  `}
`;

const Form = ({ fields, onSubmit, submitLabel, title, validationSchema }) => {
  const formComponents = {
    [TEXT_INPUT]: TextInput,
    [PASSWORD_INPUT]: PasswordInput,
  };
  const initialValueMap = fields.map((input) => ({
    [input.name]: input.initialValue,
  }));
  const initialValues = Object.assign({}, ...initialValueMap);
  const generateForm = fields.map((input) => {
    const FormComponent = formComponents[input.component];
    return (
      <Field
        component={FormComponent}
        key={input.name}
        label={input.label}
        name={input.name}
      />
    );
  });

  return (
    <StyledContainer maxWidth="xs">
      <StyledAvatar>
        <LockOutlined />
      </StyledAvatar>
      <Typography variant="h5">{title}</Typography>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {({ handleReset, handleSubmit, isSubmitting }) => (
          <form onSubmit={handleSubmit}>
            {generateForm}
            <Button
              color="primary"
              disabled={isSubmitting}
              fullWidth
              type="submit"
              variant="contained"
            >
              {submitLabel}
            </Button>
            <Button
              color="primary"
              disabled={isSubmitting}
              fullWidth
              onClick={handleReset}
            >
              Reset
            </Button>
          </form>
        )}
      </Formik>
    </StyledContainer>
  );
};

Form.propTypes = {
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      component: PropTypes.string,
      initialValue: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
      label: PropTypes.string,
      name: PropTypes.string,
    }),
  ).isRequired,
  onSubmit: PropTypes.func.isRequired,
  submitLabel: PropTypes.string,
  title: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  validationSchema: PropTypes.object,
};

Form.defaultProps = {
  submitLabel: i18n.t('submit', 'Submit'),
  title: '',
  validationSchema: null,
};

export default Form;
