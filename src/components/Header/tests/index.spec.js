import React from 'react';
import { render } from '@testing-library/react';

import Header from '..';

describe('Header', () => {
  it('renders', () => {
    const { asFragment } = render(<Header />);
    expect(asFragment()).toMatchSnapshot();
  });
});
