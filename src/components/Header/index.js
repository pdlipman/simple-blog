import React from 'react';
import { useLocation, useNavigate } from '@reach/router';

import ResponsiveNavigation from '@bit/pdlipman.react-components.responsive-navigation';

import {
  HOME_ROUTE,
  POSTS_ROUTE,
  SECURE_ROUTE,
} from 'components/App/constants';
import ResponsiveUserMenu from 'components/ResponsiveUserMenu';

const Header = () => {
  const location = useLocation();
  const { href } = location;

  const navigate = useNavigate();

  const handleClick = (value) => {
    navigate(value);
  };

  const tabs = [
    {
      key: 'home',
      label: 'Home',
      value: HOME_ROUTE,
    },
    {
      key: 'posts',
      label: 'Posts',
      value: POSTS_ROUTE,
    },
    {
      key: 'secure',
      label: 'Secure',
      value: SECURE_ROUTE,
    },
  ];
  const title = 'Simple Blog';
  return (
    <ResponsiveNavigation
      menuItems={[<ResponsiveUserMenu key="responsiveUserMenu" />]}
      onClick={handleClick}
      pathname={href}
      tabs={tabs}
      title={title}
    />
  );
};

export default Header;
