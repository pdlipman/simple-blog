import React from 'react';
import { render } from '@testing-library/react';

import UserMenu from '..';

describe('UserMenu', () => {
  it('renders', () => {
    const { asFragment } = render(<UserMenu />);
    expect(asFragment()).toMatchSnapshot();
  });
});
