import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import { IconButton, Menu, MenuItem } from '@material-ui/core';
import { AccountCircle as AccountCircleIcon } from '@material-ui/icons';

import i18n from 'i18n';

import { logoutAction } from 'providers/Firebase/Login/actions';

const UserMenu = () => {
  const [anchorElement, setAnchorElement] = useState(null);
  const dispatch = useDispatch();

  const handleClick = (event) => setAnchorElement(event.currentTarget);
  const handleClose = () => setAnchorElement(null);
  const handleLogout = () => {
    handleClose();
    dispatch(logoutAction());
  };

  return (
    <>
      <IconButton color="inherit" onClick={handleClick}>
        <AccountCircleIcon />
      </IconButton>
      <Menu
        anchorEl={anchorElement}
        keepMounted
        open={Boolean(anchorElement)}
        onClose={handleClose}
      >
        <MenuItem key="userMenuLogout" onClick={handleLogout}>
          {i18n.t('logout', 'Logout')}
        </MenuItem>
      </Menu>
    </>
  );
};

export default UserMenu;
