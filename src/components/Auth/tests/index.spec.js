import React from 'react';
import { render } from '@testing-library/react';

import Auth from '..';

describe('Auth', () => {
  it('renders', () => {
    const { asFragment } = render(<Auth />);
    expect(asFragment()).toMatchSnapshot();
  });
});
