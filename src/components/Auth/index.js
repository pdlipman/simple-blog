import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Router } from '@reach/router';

import i18n from 'i18n';

import { makeSelectUserId } from 'providers/Firebase/Auth/selectors';
import { makeSelectBack } from 'providers/Routing/selectors';
import LoginForm from './LoginForm';
import RegisterForm from './RegsiterForm';

const Auth = () => {
  const uid = useSelector(makeSelectUserId);
  const back = useSelector(makeSelectBack);

  if (uid) {
    return <Redirect to={back} noThrow />;
  }

  return (
    <>
      {i18n.t('auth', 'Auth')}
      <Router>
        <LoginForm path="login" />
        <RegisterForm path="register" />
      </Router>
    </>
  );
};

export default Auth;
