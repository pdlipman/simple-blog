import * as yup from 'yup';
import i18n from 'i18n';

import { PASSWORD_INPUT, TEXT_INPUT } from 'components/Form/constants';
import { registerRequestAction } from 'providers/Firebase/Register/actions';

const registerSchema = yup.object().shape({
  email: yup
    .string()
    .email(i18n.t('invalidEmail', 'Invalid Email'))
    .required(i18n.t('required', 'Required')),
  password: yup.string().required(i18n.t('required', 'Required')),
  confirmPassword: yup.string().when('password', {
    is: (val) => !!(val && val.length > 0),
    then: yup
      .string()
      .oneOf(
        [yup.ref('password')],
        i18n.t(
          'confirmPasswordError',
          'Password and confirmation password do not match.',
        ),
      ),
  }),
});

const register = (dispatch) => ({
  fields: [
    {
      component: TEXT_INPUT,
      initialValue: '',
      label: i18n.t('email', 'Email'),
      name: 'email',
    },
    {
      component: PASSWORD_INPUT,
      initialValue: '',
      label: i18n.t('password', 'Password'),
      name: 'password',
    },
    {
      component: PASSWORD_INPUT,
      initialValue: '',
      label: i18n.t('confirmPassword', 'Confirm Password'),
      name: 'confirmPassword',
    },
  ],
  onSubmit: (values) => dispatch(registerRequestAction({ ...values })),
  submitLabel: i18n.t('register', 'Register'),
  title: i18n.t('register', 'Register'),
  validationSchema: registerSchema,
});

export default register;
