import * as yup from 'yup';
import i18n from 'i18n';

import { PASSWORD_INPUT, TEXT_INPUT } from 'components/Form/constants';
import { loginRequestAction } from 'providers/Firebase/Login/actions';

const loginSchema = yup.object().shape({
  email: yup
    .string()
    .email(i18n.t('invalidEmail', 'Invalid Email'))
    .required(i18n.t('required', 'Required')),
  password: yup.string().required(i18n.t('required', 'Required')),
});

const login = (dispatch) => ({
  fields: [
    {
      component: TEXT_INPUT,
      initialValue: '',
      label: i18n.t('email', 'Email'),
      name: 'email',
    },
    {
      component: PASSWORD_INPUT,
      initialValue: '',
      label: i18n.t('password', 'Password'),
      name: 'password',
    },
  ],
  onSubmit: (values) => dispatch(loginRequestAction({ ...values })),
  submitLabel: i18n.t('login', 'Login'),
  title: i18n.t('login', 'Login'),
  validationSchema: loginSchema,
});

export default login;
