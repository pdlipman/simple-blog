import React from 'react';
import { useDispatch } from 'react-redux';

import Form from 'components/Form';

import { useInjectReducer } from 'hooks/useInjectReducer';
import { useInjectSaga } from 'hooks/useInjectSaga';

import loginReducer from 'providers/Firebase/Login/reducer';
import loginSaga from 'providers/Firebase/Login/saga';

import loginForm from './formTemplates/login';

const LoginForm = () => {
  useInjectReducer({ key: 'login', reducer: loginReducer });
  useInjectSaga({ key: 'login', saga: loginSaga });

  const dispatch = useDispatch();
  const form = loginForm(dispatch);
  const { fields, onSubmit, submitLabel, title, validationSchema } = form;

  return (
    <Form
      fields={fields}
      onSubmit={onSubmit}
      submitLabel={submitLabel}
      title={title}
      validationSchema={validationSchema}
    />
  );
};

export default LoginForm;
