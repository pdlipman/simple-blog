import React from 'react';
import { useDispatch } from 'react-redux';

import Form from 'components/Form';

import { useInjectReducer } from 'hooks/useInjectReducer';
import { useInjectSaga } from 'hooks/useInjectSaga';

import registerReducer from 'providers/Firebase/Register/reducer';
import registerSaga from 'providers/Firebase/Register/saga';

import registerForm from './formTemplates/register';

const RegisterForm = () => {
  useInjectReducer({ key: 'register', reducer: registerReducer });
  useInjectSaga({ key: 'register', saga: registerSaga });

  const dispatch = useDispatch();
  const form = registerForm(dispatch);
  const { fields, onSubmit, submitLabel, title, validationSchema } = form;

  return (
    <Form
      fields={fields}
      onSubmit={onSubmit}
      submitLabel={submitLabel}
      title={title}
      validationSchema={validationSchema}
    />
  );
};

export default RegisterForm;
