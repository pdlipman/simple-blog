import React from 'react';
import { render } from '@testing-library/react';

import MaterialImage from '..';

describe('MaterialImage', () => {
  it('renders', () => {
    const { asFragment } = render(<MaterialImage />);
    expect(asFragment()).toMatchSnapshot();
  });
});
