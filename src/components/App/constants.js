const BASE_ROUTE = process.env.PUBLIC_URL;
const { ASSET_PATH } = process.env;

export const HOME_ROUTE = `${BASE_ROUTE}/`;
export const LOGIN_ROUTE = `${BASE_ROUTE}/auth/login`;
export const POSTS_ROUTE = `${BASE_ROUTE}/posts`;
export const REGISTER_ROUTE = `${BASE_ROUTE}/auth/register`;
export const SECURE_ROUTE = `${BASE_ROUTE}/secure`;

export const AUTH_PATH = `${ASSET_PATH}auth/*`;
export const HOME_PATH = ASSET_PATH;
export const LOGIN_PATH = `${ASSET_PATH}auth/login`;
export const POSTS_PATH = `${ASSET_PATH}posts`;
export const POST_PATH = `${ASSET_PATH}post`;
export const REGISTER_PATH = `${ASSET_PATH}auth/login`;
export const SECURE_PATH = `${ASSET_PATH}secure`;
