import React from 'react';
import { Router } from '@reach/router';

import { CssBaseline } from '@material-ui/core';
import 'typeface-roboto';

import MainLayout from 'components/MainLayout';
import 'i18n';

const App = () => {
  console.log(process.env);
  return (
    <>
      <CssBaseline />
      <Router>
        <MainLayout path="/*" />
      </Router>
    </>
  );
};

export default App;
