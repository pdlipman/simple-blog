import React from 'react';
import { render } from '@testing-library/react';

import Posts from '..';

describe('Posts', () => {
  it('renders', () => {
    const { asFragment } = render(<Posts />);
    expect(asFragment()).toMatchSnapshot();
  });
});
