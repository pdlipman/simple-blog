import React from 'react';
import { useSelector } from 'react-redux';

import { Grid } from '@material-ui/core';

import { makeSelectPosts } from 'providers/Firebase/Post/selectors';

import PostThumbnail from './PostThumbnail';

const PostGrid = () => {
  const posts = useSelector(makeSelectPosts);
  const generateThumbnails = posts.map((post) => {
    return (
      <Grid item key={post.slug} xs={12} sm={6} md={4}>
        <PostThumbnail
          content={post.content}
          coverImage={post.coverImage}
          datetime={post.datetime}
          key={post.slug}
          slug={post.slug}
          title={post.title}
        />
      </Grid>
    );
  });

  return (
    <Grid container spacing={3}>
      {generateThumbnails}
    </Grid>
  );
};

export default PostGrid;
