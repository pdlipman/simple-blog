import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import moment from 'moment';

import styled from 'styled-components';

import { Typography } from '@material-ui/core';

import { makeSelectPostById } from 'providers/Firebase/Post/selectors';

const duration = 3000;

const StyledImage = styled.img`
  opacity: ${({ imageLoaded }) => (imageLoaded ? '1' : '0')};
  filter: saturate(${({ imageLoaded }) => (imageLoaded ? '100%' : '20%')});
  transition: opacity ${duration / 2}ms cubic-bezier(0.2, 0.6, 0.5, 0.6),
    filter ${duration}ms cubic-bezier(0.2, 0.6, 0.5, 0.6);
`;

const StyledImageContainer = styled.span`
  filter: brightness(${({ imageLoaded }) => (imageLoaded ? '100%' : '0%')});
  transition: filter ${duration * 0.75}ms cubic-bezier(0.2, 0.6, 0.5, 0.6);
`;

const Post = ({ slug }) => {
  const selectPost = useMemo(makeSelectPostById, []);
  const [isImageLoaded, setImageLoaded] = useState(false);

  const post = useSelector((state) => selectPost(state, slug));
  if (post) {
    const { content, coverImage, datetime, title } = post;
    const date = moment.unix(datetime.seconds);
    return (
      <>
        <StyledImageContainer imageLoaded={isImageLoaded}>
          <StyledImage
            alt="Cover"
            imageLoaded={isImageLoaded}
            onLoad={() => setImageLoaded(true)}
            src={coverImage}
          />
        </StyledImageContainer>
        <Typography gutterBottom variant="h5">
          {title}
        </Typography>
        <Typography color="textSecondary" gutterBottom variant="subtitle1">
          {date.format('LL')}
        </Typography>
        {/* eslint-disable-next-line react/no-danger */}
        <span dangerouslySetInnerHTML={{ __html: content }} />
      </>
    );
  }
  return <>Post not found</>;
};

Post.propTypes = {
  slug: PropTypes.string.isRequired,
};

export default Post;
