import React from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from '@reach/router';
import moment from 'moment';

import styled from 'styled-components';

import {
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Typography,
} from '@material-ui/core';

import { POSTS_ROUTE } from 'components/App/constants';

const StyledMedia = styled(CardMedia)`
  height: 15em;
`;

const StyledCopy = styled(Typography)`
  height: 5.4em;
  overflow: hidden;
  &::after {
    content: '';
    text-align: right;
    position: absolute;
    bottom: 0;
    right: 0;
    width: 70%;
    height: 2.6em;
    background: linear-gradient(to right, rgba(255, 255, 255, 0), white 100%);
    pointer-events: none;
  }
`;

const PostThumbnail = ({ content, coverImage, datetime, slug, title }) => {
  const navigate = useNavigate();
  const handleClick = () => {
    navigate(`${POSTS_ROUTE}/${slug}`);
  };

  const date = moment.unix(datetime.seconds);

  return (
    <Card variant="outlined">
      <CardActionArea onClick={handleClick}>
        {coverImage && <StyledMedia image={coverImage} />}
        <CardContent>
          <Typography gutterBottom variant="h5">
            {title}
          </Typography>
          <Typography color="textSecondary" gutterBottom variant="subtitle1">
            {date.format('LL')}
          </Typography>
          <StyledCopy color="textSecondary" variant="body2">
            {/* eslint-disable-next-line react/no-danger */}
            <span dangerouslySetInnerHTML={{ __html: content }} />
          </StyledCopy>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button>Share</Button>
        <Button onClick={handleClick}>Read</Button>
      </CardActions>
    </Card>
  );
};

PostThumbnail.propTypes = {
  content: PropTypes.string,
  coverImage: PropTypes.string,
  datetime: PropTypes.shape({ seconds: PropTypes.number.isRequired })
    .isRequired,
  slug: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

PostThumbnail.defaultProps = {
  content: '',
  coverImage: null,
};

export default PostThumbnail;
