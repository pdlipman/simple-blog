import React from 'react';
import PropTypes from 'prop-types';

import useFetching from 'hooks/useFetching';
import { postsRequestAction } from 'providers/Firebase/Post/actions';

import Post from './Post';
import PostGrid from './PostGrid';

const Posts = ({ slug }) => {
  useFetching(postsRequestAction());

  if (slug) {
    return <Post slug={slug} />;
  }

  return <PostGrid />;
};

Posts.propTypes = {
  slug: PropTypes.string,
};

Posts.defaultProps = {
  slug: null,
};

export default Posts;
