import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation, useNavigate } from '@reach/router';

import { Button, useMediaQuery } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';

import { LOGIN_PATH, LOGIN_ROUTE } from 'components/App/constants';
import UserMenu from 'components/UserMenu';
import UserList from 'components/UserList';

import { setBackRouteAction } from 'providers/Routing/actions';

import i18n from 'i18n';

import { makeSelectUserId } from 'providers/Firebase/Auth/selectors';

const ResponsiveUserMenu = () => {
  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up('sm'));

  const dispatch = useDispatch();
  const location = useLocation();
  const { href: fullPathname, pathname: route } = location;

  const uid = useSelector(makeSelectUserId);

  const MenuComponent = isDesktop ? UserMenu : UserList;

  const handleLogin = () => {
    dispatch(setBackRouteAction({ route }));
  };

  if (uid) {
    return <MenuComponent />;
  }

  return (
    <Button
      color="inherit"
      component={Link}
      disabled={fullPathname === LOGIN_ROUTE}
      fullWidth={!isDesktop}
      onClick={handleLogin}
      to={LOGIN_PATH}
    >
      {i18n.t('login', 'Login')}
    </Button>
  );
};

export default ResponsiveUserMenu;
