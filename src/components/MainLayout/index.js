import React from 'react';
import { Router } from '@reach/router';
import styled, { ThemeProvider } from 'styled-components';

import { createMuiTheme, Container, MuiThemeProvider } from '@material-ui/core';
import { blue } from '@material-ui/core/colors';

import Layout from '@bit/pdlipman.react-components.layout';

import {
  HOME_PATH,
  AUTH_PATH,
  POSTS_PATH,
  POST_PATH,
  SECURE_PATH,
} from 'components/App/constants';
import Auth from 'components/Auth';
import Header from 'components/Header';
import Home from 'components/Home';
import Posts from 'components/Posts';
import Post from 'components/Posts/Post';
import Secure from 'components/Secure';

const defaultTheme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: { main: '#fff' },
  },
});

const StyledContainer = styled(Container)`
  ${({ theme }) => `
    padding: ${theme.spacing(2)}px;
  `}
`;

const MainLayout = () => {
  return (
    <MuiThemeProvider theme={defaultTheme}>
      <ThemeProvider theme={defaultTheme}>
        <Layout>
          <Layout.Header>
            <Header />
          </Layout.Header>
          <Layout.Content>
            <StyledContainer>
              <Router>
                <Home path={HOME_PATH} />
                <Auth path={AUTH_PATH} />
                <Posts path={`${POSTS_PATH}/:slug`} />
                <Posts path={`${POSTS_PATH}`} />
                <Secure path={SECURE_PATH} />
              </Router>
            </StyledContainer>
          </Layout.Content>
          <Layout.Footer>Footer</Layout.Footer>
        </Layout>
      </ThemeProvider>
    </MuiThemeProvider>
  );
};

export default MainLayout;
