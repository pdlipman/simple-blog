import React from 'react';
import { render } from '@testing-library/react';

import MainLayout from '..';

describe('MainLayout', () => {
  it('renders', () => {
    const { asFragment } = render(<MainLayout />);
    expect(asFragment()).toMatchSnapshot();
  });
});
