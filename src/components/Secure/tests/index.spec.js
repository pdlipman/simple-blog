import React from 'react';
import { render } from '@testing-library/react';

import Secure from '..';

describe('Secure', () => {
  it('renders', () => {
    const { asFragment } = render(<Secure />);
    expect(asFragment()).toMatchSnapshot();
  });
});
