const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');

const plugins = [
  new webpack.HotModuleReplacementPlugin(),
  new Dotenv({
    path: './.env.development',
  }),
];

module.exports = require('./webpack.base')({
  output: { publicPath: '/' },
  mode: 'development',
  devServer: {
    historyApiFallback: true,
    hot: true,
    port: 3000,
  },
  plugins,
});
