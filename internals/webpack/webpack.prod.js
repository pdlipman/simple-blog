const Dotenv = require('dotenv-webpack');

const plugins = [
  new Dotenv({
    path: './.env.production',
  }),
];

module.exports = require('./webpack.base')({
  output: { publicPath: '/simple-blog/' },
  mode: 'production',
  plugins,
});
