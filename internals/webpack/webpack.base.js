const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');

const plugins = [
  new HtmlWebPackPlugin({
    inject: true,
    template: path.join(process.cwd(), 'src/index.html'),
  }),
];
module.exports = (options) => ({
  output: options.output,
  mode: options.mode,
  entry: [path.join(process.cwd(), 'src/index.js')],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(eot|otf|ttf|woff|woff2)$/,
        use: 'file-loader',
      },
    ],
  },
  devServer: options.devServer,
  plugins: options.plugins.concat(plugins),
  resolve: {
    alias: {
      components: path.resolve(__dirname, '../../src/components'),
      hooks: path.resolve(__dirname, '../../src/hooks'),
      i18n: path.resolve(__dirname, '../../src/i18n'),
      providers: path.resolve(__dirname, '../../src/providers'),
      store: path.resolve(__dirname, '../../src/store'),
    },
  },
});
