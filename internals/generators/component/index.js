const componentExists = require('../utils/componentExists');

module.exports = {
  description: 'Create a component',
  // User input prompts provided as arguments to the template
  prompts: [
    {
      // Raw text input
      type: 'input',
      // Variable name for this input
      name: 'name',
      // Prompt to display on command line
      message: 'What is your component name?',
      validate: (value) => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? 'A component or container with this name already exists'
            : true;
        }

        return 'The name is required';
      },
    },
    {
      type: 'confirm',
      name: 'wantTests',
      default: true,
      message: 'Add tests?',
    },
  ],
  actions: (data) => {
    const actions = [
      {
        // Add a new file
        type: 'add',
        // Path for the new file
        path: '../../src/components/{{properCase name}}/index.js',
        // Handlebars template used to generate content of new file
        templateFile: './component/index.js.hbs',
        abortOnFail: true,
      },
    ];

    if (data.wantTests) {
      actions.push({
        type: 'add',
        path: '../../src/components/{{properCase name}}/tests/index.spec.js',
        templateFile: './component/index.spec.js.hbs',
        abortOnFail: true,
      });
    }

    actions.push({
      type: 'prettify',
      path: '/src/components/',
      abortOnFail: true,
    });

    return actions;
  },
};
